import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import CustomModal from './components/modal';
import { createTodo, deleteTodo, updateTodo } from './features/todoSlice';

function App() {
	const todos = useSelector(state => state.todo.todos);
	const dispatch = useDispatch();
	const [todo, setTodo] = useState([]);
	const [updatedTodo, setUpdatedTodo] = useState('');
	const [selectedTodo, setSelectedTodo] = useState({});
	const [modalUpdate, setModalUpdate] = useState(false);

	return (
		<main className="max-w-2xl mx-auto w-11/12">
			<h2 className="text-center text-xl mt-3">Todolist</h2>

			<input type="text" className="w-full border border-sky-500 mt-5" value={todo} onChange={e => setTodo(e.target.value)} />
			<button
				className="bg-sky-500 text-white py-1 px-3 rounded-md mt-2"
				onClick={() => {
					dispatch(createTodo(todo));
					setTodo('');
				}}
			>
				Submit
			</button>

			<h5 className="mt-8 mb-2 text-lg">List Todo</h5>
			<ul>
				{todos.length
					? todos.map(todo => (
							<li key={todo.id} className="list-disc space-x-4">
								<span>{todo.title}</span>
								<button className="bg-red-500 text-white py-1 px-2 text-sm rounded-md" onClick={() => dispatch(deleteTodo(todo.id))}>
									Delete
								</button>
								<button
									className="bg-green-500 text-white py-1 px-2 text-sm rounded-md"
									onClick={() => {
										setSelectedTodo(todo);
										setModalUpdate(true);
										setUpdatedTodo(todo.name);
									}}
								>
									Update
								</button>
							</li>
					  ))
					: 'No todo in here'}
			</ul>

			{/* Modal */}
			<CustomModal isOpen={modalUpdate} title="Update todo" closeModal={() => setModalUpdate(false)}>
				<input type="text" className="w-full border border-sky-500 mt-5" value={updatedTodo} onChange={e => setUpdatedTodo(e.target.value)} />
				<button
					className="bg-sky-500 text-white py-1 px-3 rounded-md mt-2"
					onClick={() => {
						dispatch(updateTodo({ id: selectedTodo.id, updateTitle: updatedTodo }));
						setModalUpdate(false);
					}}
				>
					Update
				</button>
			</CustomModal>
		</main>
	);
}

export default App;
