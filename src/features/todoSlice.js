import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	todos: [],
};

export const todoSlice = createSlice({
	name: 'todo',
	initialState,
	reducers: {
		createTodo: (state, action) => {
			state.todos = [...state.todos, { id: Date.now(), title: action.payload }];
		},
		deleteTodo: (state, action) => {
			const filteredTodo = state.todos.filter(todo => todo.id !== action.payload);

			state.todos = filteredTodo;
		},
		updateTodo: (state, action) => {
			const findTodo = state.todos.filter(todo => todo.id === action.payload);
			findTodo.title = action.payload.updateTitle;

			const filteredTodo = state.todos.filter(todo => todo.id !== action.payload.id);
			state.todos = [...filteredTodo, findTodo];
		},
	},
});

// Action creators are generated for each case reducer function
export const { createTodo, deleteTodo, updateTodo } = todoSlice.actions;

export default todoSlice.reducer;
